Hosting server titles
=====================

This is a really simple module that allows you to change the names of servers
that are displayed in lists in the Hosting frontend.

This allows you to do lovely things like give your servers a 'Friendly name'
that appears when choosing what database server to use for a site. Handy!

This module is a simple bridge between Aegir and token, and as such you'll want
to install [token](http://drupal.org/project/token) module also.

Installation
------------

This module requires hostmaster version 6.x-1.2 or above, or you can apply these
two simple patches:
http://drupalcode.org/project/hostmaster.git/patch/22ec74d44e73557ccab9b09d7809fecfed200713
http://drupalcode.org/project/hostmaster.git/patch/6ed45f3bd56aa8d582379cdb2c5f536d228c932e

Install and enable like any other module, note that you'll also need the [Token
module](http://drupal.org/project/token).

Configuration
-------------

After enabling the module, you can configure the pattern to use for the servers
at the admin page found at: `admin/hosting/hosting_server_titles`

Examples
--------

To get a simple friendly name for servers set up, install
[CCK](http://drupal.org/project/cck) and enable the Text module. Add a simple
text field to your server content type, called 'Friendly name'. You can then set
the server name pattern to something like:

    [field_friendly_name-raw] ([title-raw])

And you'll hopefully never get confused about which server is which again!
