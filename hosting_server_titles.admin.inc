<?php

/**
 * @file
 * Admin related bits for the Hosting server titles module.
 */

/**
 * Setting form for Hosting server titles module.
 */
function hosting_server_titles_settings_form() {
  $form = array();

  $form['hosting_server_titles_pattern_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Pattern for servers'),
    '#default_value' => variable_get('hosting_server_titles_pattern_default', ''),
    '#description' => t('This pattern will be used to display the server when for example: choosing a webserver to deploy a platform on; choosing a database server for a site. If you leave this empty, no changes to titles will be made from the Hosting default (which is equivalent to [title-raw]).'),
  );

  $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['token_help']['help'] = array(
    // Oh yeah, support the new theme_token_tree, but fall back to theme_token_help.
    '#value' => theme(array('token_tree', 'token_help'), array('node', 'global')),
  );

  return system_settings_form($form);
}
